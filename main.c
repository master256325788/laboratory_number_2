#include <stdio.h>
#include <stdint.h>

int uint16ToBin(uint16_t value){ // вывод двоичного числа
    printf("&:{");
    int bite16 = 32768;
    while (bite16 >= 1){
	if (value & bite16 ){
	    printf("1");
	}else{
	    printf("0");
	}
	bite16 /= 2;
    }
    printf("}");
    return 0;
}


int main(){ // начало основного цикла
    const int Replace_Bit_First = 4;
    const int Replace_Second = 16;
    int arrayBeforeSum;
    int arrayAfterSum;
    int count;
    printf("введите колличество элементов массива:");
    scanf("%d", &count);

    uint16_t arrayBefore[count];
    uint16_t arrayAfter[count];

    for (int i = 0; i < count; i++){
	printf("введите элемент массива N%d:", i + 1);
	scanf("%hd", &arrayBefore[i]);
	arrayBeforeSum += arrayBefore[i];
	arrayAfter[i] = arrayBefore[i];
    }
    for (int i = 0; i < count; i++){
	if (arrayBefore[i] & Replace_Bit_First){
	    if (arrayBefore[i] & Replace_Second){}
	    else{
		arrayAfter[i] += Replace_Second;
		arrayAfter[i] -= Replace_Bit_First;
	    }
	}
	else{
	    if (arrayBefore[i] & Replace_Second){
		arrayAfter[i] += Replace_Bit_First;
		arrayAfter[i] -= Replace_Second;
	    }
	    else{}
	}
    }
    for (int i = 0; i < count; i++){
    printf("\nЧисло до перемещения разрядов:\n");
    printf("Dec:%hd  ", arrayBefore[i]);
    uint16ToBin(arrayAfter[i]);
    printf("\nЧисло после перемещения разрядов:\n");
    printf("Dec:%hd  ", arrayAfter[i]);
    uint16ToBin(arrayBefore[i]);
    printf("\n");
    }
    for (int i = 0; i < count; i++){
	arrayAfterSum += arrayAfter[i];
    }
    printf("\nСумма чисел до перемещения разрядов:%d\n", arrayBeforeSum);
    printf("Сумма чисел после перемещения разрядов:%d\n", arrayAfterSum);
    return 0;

}

